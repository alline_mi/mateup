from django.db.models import Sum
from django.forms import modelformset_factory, inlineformset_factory
from django.http import HttpResponseRedirect
from django.views import generic
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, FormView
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from django.views.generic.detail import DetailView

from mateupsite.matching import MatchCandidateAttributes, MatchAlgorithm
from .models import Profile, Accommodation, AccommodationImage, Attribute
from .forms import ProfileForm, CreateAccommodationForm, AccommodationImageForm, ProfileUserFormSet, UserProfileForm, \
    UpdateAccommodationForm
from django.views.generic.edit import CreateView


# Create your views here.
class IndexPageView(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)


class RegisterPageView(TemplateView, generic.FormView):
    # Related to the data validation
    form_class = UserCreationForm
    template_name = "register.html"
    success_url = "/"

    def get(self, request, **kwargs):
        the_form = UserCreationForm()
        return render(request, self.template_name, context={"form": the_form})

    def post(self, request, *args, **kwargs):
        the_form = UserCreationForm(data=request.POST)
        if the_form.is_valid():
            # this save() creates a new instance of the db model
            u = the_form.save()
            # saves to db
            u.save()
            return redirect('index')
        else:
            return render(request, self.template_name, context={"form": the_form})




# http://kevindias.com/writing/django-class-based-views-multiple-inline-formsets/
class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'profile.html'
    form_class = UserProfileForm
    model = User

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        read_only = False
        if self.kwargs.get('userId', None):
            read_only = True

        if self.request.POST:
            context['user_form'] = UserProfileForm(self.request.POST, instance=self.object)
            context['profile_form'] = ProfileForm(self.request.POST, instance=Profile.objects.get(user=self.object))
            context['is_readonly'] = read_only
        else:
            context['user_form'] = UserProfileForm(instance=self.object)
            context['profile_form'] = ProfileForm(instance=Profile.objects.get(user=self.object))
            context['is_readonly'] = read_only
        return context

    def get_object(self, queryset=None):
        if self.kwargs.get('userId', None):
            return User.objects.get(id=self.kwargs.get('userId'))
        return self.request.user

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        user_form = UserProfileForm(self.request.POST, instance=self.object)
        profile_form = ProfileForm(self.request.POST, instance=Profile.objects.get(user=self.object))

        if user_form.is_valid() and profile_form.is_valid() and self.request.user==self.object:
            user = user_form.save(commit=False)
            profile = profile_form.save(commit=False)
            profile.attributes.clear()
            selected_attributes = request.POST.getlist('attributes')
            for att in selected_attributes:
                profile.attributes.add(Attribute.objects.get(id=int(att)))

            profile.save()
            user.save()

        return render(self.request, template_name=self.template_name, context=self.get_context_data(**kwargs))





class MatchPageView(LoginRequiredMixin, TemplateView):
    template_name = 'match.html'
    raise_exception = True  # raise a 403 error instead of redirecting

    def get(self, request, **kwargs):
        # for this initial version we only want the intersection of the several algorithms
        profile = Profile.objects.get(user=self.request.user)
        matches = MatchAlgorithm.combine_match([MatchCandidateAttributes()], profile, result_as_union=False)
        return render(request, self.template_name, context={'matches': matches})



class CreateAccommodationView(LoginRequiredMixin, CreateView):
    model = Accommodation
    form_class = CreateAccommodationForm
    fields = ['address', 'description', 'images']

    template_name = 'accommodation_form.html'
    #form_class = CreateAccommodationForm
    success_url = '...'  # Replace with your URL or reverse().

    def get(self, request, **kwargs):
        the_form = CreateAccommodationForm()

        ImageFormSet = modelformset_factory(AccommodationImage, form=AccommodationImageForm, extra=3)
        img_formset = ImageFormSet(queryset=AccommodationImage.objects.none())

        return render(request, self.template_name, context={"form": the_form, "img_formset": img_formset})


    def post(self, request, *args, **kwargs):
        # from http://stackoverflow.com/questions/34006994/how-to-upload-multiple-images-to-a-blog-post-in-django
        ImageFormSet = modelformset_factory(AccommodationImage, form=AccommodationImageForm, extra=3)

        the_form = CreateAccommodationForm(request.POST or None)
        img_formset = ImageFormSet(request.POST or None, request.FILES or None, queryset=AccommodationImage.objects.none())
        if the_form.is_valid() and img_formset.is_valid():
            # this save() creates a new instance of the db model
            accommodation = the_form.save(commit=False)
            existing = Accommodation.objects.filter(user=request.user, enabled=True).first()
            if existing:
                existing.enabled=False
                existing.save()

            accommodation.enabled = True
            accommodation.user = User.objects.get(id=request.user.id)
            accommodation.save()

            for form in img_formset.cleaned_data:
                if 'images' in form.keys():
                    image = form['images']
                    photo = AccommodationImage(accommodation=accommodation, images=image)
                    photo.save()

            return redirect(reverse('accommodation', args=(accommodation.id,)))
        else:
            return render(request, self.template_name, context={"form": the_form})



class AccommodationDetailsView(LoginRequiredMixin, TemplateView):
    template_name = 'accommodation_details.html'

    def get(self, request, *args, **kwargs):
        accom_id = int(kwargs.get('pk'))
        accommodation = Accommodation.objects.get(id=accom_id)
        images = list(AccommodationImage.objects.filter(accommodation=accommodation))
        profile = Profile.objects.get(user=accommodation.user)

        imgs = []
        for img in images:
            imgs.append(dict(
                url=img.images.url,
                caption=img.caption
            ))

        profile_view = dict(
            email=profile.user.email,
            attributes=[dict(name=a.name) for a in list(profile.attributes.all())],
            avatar='/static/default_avatar.png',
            fullname=profile.user.get_full_name()
        )

        if profile.avatar.__nonzero__():
            profile_view['avatar'] = profile.avatar.url

        accom = dict(
            description=accommodation.description,
            address=accommodation.address,
        )
        context = {}
        context['imgs'] = imgs
        context['accom'] = accom
        context['profile'] = profile_view

        return render(self.request, template_name=self.template_name, context=context)

        # if request.method == 'POST':
        #
        #     form_class = self.get_form_class()
        #     form = self.get_form(form_class)
        #     address = request.POST['address']
        #     description = request.POST['description']
        #     files = request.FILES.getlist('images')
        #
        #     if form.is_valid():
        #         for f in files:
        #             instance = Accommodation(
        #                 address=address,description=description,
        #                 images=f
        #             )
        #             # file is saved with model
        #             instance.save()
        #
        #     return redirect ('/')
        # else:
        #     form = CreateAccommodationForm()
        # return render(request, 'accommodation_form.html', {'form': form})


# url(r'^accommodation/(?P<accommId>\d+)$', views.AccommodationDisplayView.as_view(), name='accommodation'),
class AccommodationDisplayView(TemplateView):
    template_name = "tests/accomodation_display_test.py.html"

    def get(self, request, accommId):
        acc = get_object_or_404(Accommodation, id=accommId)
        imgs = acc.accommodationimage_set.all()
        return render(request, self.template_name, context={'accom': acc, 'imgs':imgs})



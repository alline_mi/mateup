from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from mateupsite import views

urlpatterns = [
                  url(r'^$', views.IndexPageView.as_view(), name='index'),
                  url(r'^register/', views.RegisterPageView.as_view(), name='register'),
                  url(r'^login/', auth_views.login, {'template_name': 'login.html'}, name='login'),
                  url(r'^logout/', auth_views.logout, name='logout'),


                  # used in the matching

                  url(r'^profile/(?P<userId>\d+)$', views.ProfileView.as_view(), name='profile'),
                  url(r'^profile/', views.ProfileView.as_view(), name='profile'),

                  url(r'^match/', views.MatchPageView.as_view(), name='match'),

                  # add accommodation url
                  # url(r'^accommodation/(?P<accommId>\d+)$', views.AccommodationDisplayView.as_view(), name='accommodation'),
                  url(r'^accommodation/(?P<pk>\d+)$', views.AccommodationDetailsView.as_view(), name='accommodation'),

                  url(r'^accommodation/add/', views.CreateAccommodationView.as_view(), name='accommodation-add'),



                  # to run properly, auth_views require that password url reset was created, otherwise it would display errors.
                  # page was not created.
                  url(r'^password_reset/', auth_views.password_reset, name='password_reset'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

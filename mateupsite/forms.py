from __future__ import unicode_literals
from django import forms
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from django.utils.translation import ugettext_lazy as _
from .models import Profile, Accommodation, AccommodationImage, Attribute


class UserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"), strip=False, widget=forms.PasswordInput,)
    password2 = forms.CharField(label=_("Password confirmation"), widget=forms.PasswordInput, strip=False, help_text=_(
        "Enter the same password as before, for verification."), )
    first_name = forms.CharField(label=_("First name"))

    class Meta:
        model = User
        fields = ("username", "first_name")

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        if self._meta.model.USERNAME_FIELD in self.fields:
            self.fields[self._meta.model.USERNAME_FIELD].widget.attrs.update({'autofocus': ''})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')

        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class ProfileForm(forms.ModelForm):
    # FIXME = ALLINE to remove the link below the photo, but keep the 'clear' checkbox
    # FIXME = ALLINE https://github.com/django/django/blob/339c01fb7552feb8df125ef7e5420dae04fd913f/django/forms/widgets.py#L434
    # FIXME = ALLINE avatar = forms.ImageField(widget=forms.ClearableFileInput)
      class Meta:
        model = Profile
        fields = ['avatar', 'address', 'searching', 'attributes']
        widgets = {'attributes': forms.CheckboxSelectMultiple,
                   'address': forms.Textarea(attrs={'cols': 80, 'rows': 5, 'class': 'form-control profile-form-control'}),
                   }

    #     # to display the attributes with a checkbox beside them
    #     # FIXME the ''avatar': forms.FileInput' will remove the link below the photo, but will also remove the 'clear' checkbox. We don't want this. Research more.
    #     widgets = {'attributes': forms.CheckboxSelectMultiple,
    #                 'address': forms.Textarea(attrs={'cols': 80, 'rows': 5, 'class':'form-control profile-form-control'}),
    #                 'email': forms.TextInput(attrs={'class': 'form-control profile-form-control', })
    #                }  # , 'avatar': forms.FileInput}
    #     # the order of the attributes here affect the order that they are going to appear on profile.html

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = User
        exclude = ['is_active', 'date_joined', 'password', 'username']
        widgets = {
            'email': forms.TextInput(attrs={'class': 'form-control profile-form-control', }),
            'first_name': forms.TextInput(attrs={'class': 'form-control profile-form-control', }),
            'last_name': forms.TextInput(attrs={'class': 'form-control profile-form-control', }),
        }

ProfileUserFormSet = inlineformset_factory(User, Profile, exclude=[])


class CreateAccommodationForm(forms.ModelForm):

    address = forms.CharField(max_length=1000, label=_("Address"))
    description = forms.CharField(label=_("Description"), widget=forms.Textarea)
    #images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=False)
    #images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Accommodation
        fields = ("address", "description")

    def save(self, commit=True):
        user = super(CreateAccommodationForm, self).save(commit=False)

        if commit:
            user.save()
        return user

class UpdateAccommodationForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(UpdateAccommodationForm, self).__init__(*args, **kwargs)

    address = forms.CharField(max_length=1000, label=_("Address"))
    description = forms.CharField(label=_("Description"), widget=forms.Textarea)
    #images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}), required=False)
    #images = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    class Meta:
        model = Accommodation
        fields = ("address", "description")

    def save(self, commit=True):
        user = super(UpdateAccommodationForm, self).save(commit=False)

        if commit:
            user.save()
        return user

class AccommodationImageForm(forms.ModelForm):
    class Meta:
        model = AccommodationImage
        fields = ('images', 'caption')
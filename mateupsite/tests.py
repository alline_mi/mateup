from importlib import import_module
from django.conf import settings

from django.contrib.auth.models import User
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import TestCase, RequestFactory, LiveServerTestCase, Client, override_settings

# Create your tests here.
from django.urls import reverse
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.wait import WebDriverWait

from mateupsite.matching import intersect_lists, union_lists, MatchAlgorithm, MatchCandidateAttributes

# https://docs.djangoproject.com/en/1.11/topics/testing/advanced/
from mateupsite.models import Profile, Attribute


class MatchingAlgorithmListTests(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()


    def test_intersect_lists_empty_lists(self):
        results = intersect_lists([], [1, 2, 3])
        self.assertEqual(results, [], 'intersect_lists should return an empty list if one list is empty')
        results = intersect_lists(None, [1, 2, 3])
        self.assertEqual(results, [], 'intersect_lists should return an empty list if one list is empty')
        results = intersect_lists([1, 2], [])
        self.assertEqual(results, [], 'intersect_lists should return an empty list if one list is empty')
        results = intersect_lists([1, 2, 3], None)
        self.assertEqual(results, [], 'intersect_lists should return an empty list if one list is empty')

    def test_intersect_lists(self):
        expected = [1]
        list_a = [1, 3]
        list_b = [1, 2]
        results = intersect_lists(list_a, list_b)
        self.assertEqual(results, expected)

    def test_union_lists_empty_lists(self):
        results = union_lists([], [1, 2, 3])
        self.assertEqual(results, [1, 2, 3])
        results = union_lists(None, [1, 2, 3])
        self.assertEqual(results, [1, 2, 3])

        results = union_lists([1, 2, 3], [])
        self.assertEqual(results, [1, 2, 3])
        results = union_lists([1, 2, 3], None)
        self.assertEqual(results, [1, 2, 3])

    def test_union_lists(self):
        expected = [1, 2, 3, 4]
        list_a = [1, 3]
        list_b = [2, 3, 4]
        result = union_lists(list_a, list_b)
        self.assertEqual(result, expected)



class MatchingAlgorithmTests(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.create_attributes()
        self.request_user = User.objects.create_user(username='alline',
                                                     email='alline@email.com',
                                                     password='password')
        self.request_user.save()
        self.profile = Profile.objects.get(user=self.request_user)
        self.profile.attributes.add(Attribute.objects.get(name='Pets allowed'))
        self.profile.attributes.add(Attribute.objects.get(name='Smoking'))
        self.profile.save()

    def create_attributes(self):
        Attribute.objects.create(name='Pets allowed', value=1, enabled=True).save()
        Attribute.objects.create(name='Smoking', value=1, enabled=True).save()
        Attribute.objects.create(name='Guests allowed', value=1, enabled=True).save()
        Attribute.objects.create(name='Drinking allowed', value=1, enabled=True).save()


    def test_matching_by_user_attributes(self):
        test_user_1 = User.objects.create_user(username='rafael',
                                               email='rafael@email.com',
                                               password='password')
        test_profile_1 = Profile.objects.get(user=test_user_1)
        test_profile_1.searching = False
        test_profile_1.attributes.add(Attribute.objects.get(name='Drinking allowed'))
        test_profile_1.save()

        results = MatchAlgorithm.combine_match([MatchCandidateAttributes()],
                                               self.profile,
                                               result_as_union=False)

        self.assertEqual(results, [])

        test_user_2 = User.objects.create_user(username='Eduarda',
                                               email='eduarda@email.com',
                                               password='password')
        test_profile_2 = Profile.objects.get(user=test_user_2)
        test_profile_2.searching = False
        test_profile_2.attributes.add(Attribute.objects.get(name='Pets allowed'))
        test_profile_2.save()

        results = MatchAlgorithm.combine_match([MatchCandidateAttributes()],
                                               self.profile,
                                               result_as_union=False)

        # users share a common interest
        self.assertEqual(results, [dict(accommodation=None, candidate=test_profile_2)])

        test_user_3 = User.objects.create_user(username='Andre',
                                               email='andre@email.com',
                                               password='password')
        test_profile_3 = Profile.objects.get(user=test_user_3)
        test_profile_3.searching = False
        test_profile_3.attributes.add(Attribute.objects.get(name='Pets allowed'))
        test_profile_3.attributes.add(Attribute.objects.get(name='Smoking'))
        test_profile_3.attributes.add(Attribute.objects.get(name='Guests allowed'))
        test_profile_3.attributes.add(Attribute.objects.get(name='Drinking allowed'))
        test_profile_3.save()

        results = MatchAlgorithm.combine_match([MatchCandidateAttributes()],
                                               self.profile,
                                               result_as_union=False)

        # order is important
        self.assertEqual(results, [dict(accommodation=None, candidate=test_profile_3),
                                   dict(accommodation=None, candidate=test_profile_2)])


class ViewsTestCase(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.request_user = User.objects.create_user(username='alline',
                                                     email='alline@email.com',
                                                     password='password')

    @override_settings(SECURE_SSL_REDIRECT=False)
    def test_profile_view(self):
        resp = self.client.get('/profile/')
        # the user is not authenticated so the request has to be redirected to /login/?next=/profile/
        # for the user to put in their authentication
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(resp._headers['location'][1], '/login/?next=/profile/')



##########################
# Functional testing
##########################

# from https://docs.djangoproject.com/en/1.11/topics/testing/tools/
@override_settings(SECURE_SSL_REDIRECT=False)
class LoginSeleniumTestCase(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super(LoginSeleniumTestCase, cls).setUpClass()
        cls.username = 'alline'
        cls.password = '_@ssw0rd1!'
        cls.request_user = User.objects.create_user(username=cls.username,
                                                     email='alline@email.com',
                                                     password=cls.password)

        # we were getting the following error on pipelines
        #  WebDriverException: Message: unknown error: Chrome failed to start: exited abnormally
        # following the solution mentioned here:
        # http://stackoverflow.com/questions/22424737/unknown-error-chrome-failed-to-start-exited-abnormally
        from pyvirtualdisplay import Display
        display = Display(visible=0, size=(800, 800))
        display.start()
        # http://stackoverflow.com/questions/8220108/how-do-i-check-the-operating-system-in-python
        from sys import platform
        if platform == "linux" or platform == "linux2":
            chrome_options = Options()
            chrome_options.add_argument("--no-sandbox")
            chrome_options.add_argument("--disable-setuid-sandbox")
            cls.selenium = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=chrome_options)

        elif platform == "win32":
            cls.selenium = webdriver.Chrome()
        cls.selenium.implicitly_wait(10)


    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(LoginSeleniumTestCase, cls).tearDownClass()


    def test_open_login_directly(self):
        self.selenium.get('%s%s' % (self.live_server_url, "/login/"))

        id_username_control = 'id_username'
        id_password_control = 'id_password'

        username = self.selenium.find_element_by_id(id_username_control)
        password = self.selenium.find_element_by_id(id_password_control)

        username.send_keys(self.username)
        password.send_keys(self.password)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # we are now authenticated, let's see if the message in the main page is accurate with the new logon status
        self.selenium.get('%s' % (self.live_server_url))
        user_area_div = self.selenium.find_element_by_class_name('user-area')
        self.assertEqual(user_area_div.text, 'Hi, alline Logout')

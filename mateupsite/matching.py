from abc import abstractmethod
from abc import ABCMeta

from django.db.models import Sum

from mateupsite.models import Accommodation, Profile



# DOC
# https://docs.python.org/2/library/abc.html
# A class that has a metaclass derived from ABCMeta cannot be instantiated unless all of its abstract methods and
# properties are overridden. The abstract methods can be called using any of the normal 'super' call mechanisms.
#

# based on http://www.saltycrane.com/blog/2008/01/how-to-find-intersection-and-union-of/
def intersect_lists(list_a, list_b):
    """
    Returns the intersection between two lists. If one of the lists is empty or null then the function returns 
    an empty list. 
     
    :param list or None list_a: 
    :param list or None list_b: 
    :return: 
    """
    if not list_a or not list_b:
        return []

    return list(set(list_a) & set(list_b))


def union_lists(list_a, list_b):
    if not list_a and list_b:
        return list_b
    if not list_b and list_a:
        return list_a
    if list_a and list_b:
        return list(set(list_a) | set(list_b))
    return []


class MatchAlgorithm(object):
    """
    Abstract base class for all matching algorithms, example:
        1. by users attributes/preferences
        2. by accommodation type being search vs being offered
    
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def match(self, user):
        """
        Abstract method.
        
        :param User user: the user to which we want to find matches 
        :param User user: 
        :return: returns a list of dictionaries. Each dictionary has a candidate and accommodation offered.
        :rtype: list of dict
        """
        pass

    @staticmethod
    def combine_match(algorithms, user, result_as_union=False):
        """
        
        
        :param list of MatchAlgorithm algorithms: 
        :param User user: 
        :param bool result_as_union: is the result the union or the intersection of the several algorithms being applied  
        :return: 
        """
        matches = []
        for algorithm in algorithms:
            results = algorithm.match(user)
            if not result_as_union and matches: # intersection
                matches = intersect_lists(matches, results)
            else:
                matches = union_lists(matches, results)
        return matches



class MatchCandidateAttributes(MatchAlgorithm):

    def match(self, profile):
        """
        Finds the users with the same preferences using the attributes field
        
        :param Profile user: 
        :return: returns a list of dictionaries. Each dictionary has a candidate and accommodation offered.
        :rtype: list of dict
        """
        desired_attributes = profile.attributes.filter(enabled=True)  # Consider only enabled attribs

        # Will exclude the user with same search filter
        # Avoid searching for ourselves or users not offering accomodation
        # All users with at least one attr in common with the user
        candidates = Profile.objects.exclude(pk=profile.pk).exclude(searching=profile.searching)
        candidates = candidates.filter(attributes__in=desired_attributes)
        # This is the most important part, the candidates are evaluated by the sum of the value of
        # the attributes they share with the user
        candidates = candidates.annotate(proximity=Sum('attributes__value'))
        candidates_match = candidates.order_by('-proximity')  # Best matches first

        matches = []
        for candidate in candidates_match:
            accomodation = list(Accommodation.objects.filter(user=candidate.user, enabled=True))

            matches.append({
                'candidate': candidate,
                'accommodation': accomodation[0] if accomodation else None,
            })
        return matches

from django.contrib import admin
from .models import Attribute, Profile, UserPreferences, Accommodation



# Register your models here.
@admin.register(Attribute)
class AttributeAdmin(admin.ModelAdmin):
    list_display = ('name', 'enabled')


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    filter_horizontal = ('attributes',)


# To ease admin access during production
class UserPreferencesAdmin(admin.ModelAdmin):
    pass

admin.site.register(UserPreferences, UserPreferencesAdmin)

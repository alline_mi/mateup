from __future__ import unicode_literals
from django.db import models
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


# Custom profile model that extends Django's User with a MateUp user profile information, based on:
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(blank=True, upload_to='avatar', null=True)
    email = models.EmailField(max_length=255, blank=True)
    address = models.TextField(max_length=500, blank=True)  # , help_text='help text appears below address textfield')
    searching = models.BooleanField(default=True, verbose_name='Looking for Accommodation: ')
    attributes = models.ManyToManyField('Attribute', blank=True)

    @property
    def name(self):
        return self.user.get_full_name()

    def __str__(self):
        return '{} ({})'.format(self.name, self.user.username)


# Basically we are hooking the create_user_profile and save_user_profile methods to the User model, whenever a save
# event occurs.
@receiver(models.signals.post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(models.signals.post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# user preferences for compatibility match
class UserPreferences(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.user)


# Related to the matching algorithm
class Attribute(models.Model):
    name = models.CharField(max_length=255)
    value = models.FloatField(default=1)  # Relevance of the attr. Will allow for more accurate (weighted) matches to be implemented in the future
    enabled = models.BooleanField(default=False)  # is offering or looking accommodation boolean
    description = models.TextField(max_length=800, blank=True)

    def __str__(self):
        return self.name


# related to Accommodation model Images
def upload_location(instance, filename):
    return "%s/%s" % (instance.user.id, filename)

def upload_accommodation_image_location(instance, filename):
    return "accommodation_imgs/%s/%s" % (instance.accommodation.id, filename)


# 31/03/2017
# model Accommodation--possibly move this to it relevant app
class Accommodation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    address = models.CharField(max_length=500, blank=True)
    description = models.TextField(max_length=500, blank=True)
    # images attribute, more than one pictures from the house, many to many fields??
    # images = models.ImageField('Images', blank=True)
    enabled = models.BooleanField(default=False)

    # function to redirect to accommodation post as soon as it is created
    def get_absolute_url(self):
        return reverse('mateupsite:accommodation', kwargs={'pk': self.pk})

    def __str__(self):
        return self.address

    def create(self, address, description):
        accommodation = self.create(address=address, description=description)
        return accommodation

class AccommodationImage(models.Model):
    accommodation = models.ForeignKey(Accommodation, on_delete=models.CASCADE)
    images = models.ImageField(upload_to=upload_accommodation_image_location, null=True, blank=True,
                               width_field="width_field",
                               height_field="height_field")
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    caption = models.CharField(max_length=100)

# TODO: make sure that is being used, remove if not
# class Images(models.Model):
#     images = models.ImageField(upload_to=upload_location, null=True, blank=True,
#                                width_field="width_field",
#                                height_field="height_field")
#     height_field = models.IntegerField(default=0)
#     width_field = models.IntegerField(default=0)
#     caption = models.CharField(max_length=100)




# ########### maybe we need this on accommodation
    # https://developers.google.com/maps/documentation/geocoding/start
    # address_street = models.CharField(max_length=1000)
    # address_postal_code = models.CharField(max_length=30)
    # https://docs.djangoproject.com/en/1.10/ref/contrib/gis/model-api/

    # location = gis_models.PointField(null=True)
